myForm.addEventListener('submit', function() {
	event.preventDefault();
	console.log('submit');
	
	send.disabled = "true";
	send.classList.add("progress");
	send.value = "отправка...";
	
	let	name = document.getElementById('name').value;
	let	email = document.getElementById('email').value;
	let	phone = document.getElementById('phone').value;
	let	bdate = document.getElementById('bdate').value;
	let	message = document.getElementById('message').value;

	const request = new XMLHttpRequest();
	const url = "server.php";
	 
	const params = "name=" + name+ "&email=" + email+ "&phone=" + phone+ "&bdate=" + bdate+ "&message=" + message;
	 
	request.open("POST", url, true);
	 
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	 
	request.addEventListener("readystatechange", () => {
		if (request.readyState != 4) return;
		if(request.status === 200 && request.responseText == "true") { 
				
				send.classList.add("success");
				send.value = "Готово!";
				console.log(request.responseText);
		}
		else
		{
				send.classList.add("error");
				send.value = "Произошла ошибка";
				console.log(request.responseText);
		}
	});
	 
	request.send(params); 
})



